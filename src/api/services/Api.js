import axios from 'axios';
import { appConfig } from 'config';

export default class Api {
    _doGetRequest(route, model, config = {}) {
        let request = config
            ? axios.get(appConfig.API.URL + route, config)
            : axios.get(appConfig.API.URL + route);
        return request
            .then(response => this._convertResponseToModel(response, model));
    }

    _doPostRequest(route, model, config = {}) {
        let request = axios.post(appConfig.API.URL + route, config);
        return request
            .then(response => this._convertResponseToModel(response, model));
    }

    _convertResponseToModel(response, model) {
        const data = response.data;

        if (!model) {
            return data;
        }

        if (Array.isArray(data)) {
            return data.map(el => model(el));    
        }

        return model(data);
    }

    isValidJson(json) {
        try {
            JSON.parse(json);
            return true;
        } catch (e) {
            return false;
        }
    }
}