import { appConfig } from 'config';
import Api from './Api';

class JWTBApi extends Api {
    get() {
        return this._doGetRequest(appConfig.API.ROUTES.JWTB, null);
    }
}

export default JWTBApi;