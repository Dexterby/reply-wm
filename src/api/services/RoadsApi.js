import { appConfig } from 'config';
import Api from './Api';

class RoadsApi extends Api {
    getRoads() {
        return this._doGetRequest(appConfig.API.ROUTES.ROAD, null);
    }
}

export default RoadsApi;