import React, { Component } from 'react';
import './index.scss';

class Dashboard extends Component {
  render() {
    return (
      <div className="Dashboard">
        404 NOT FOUND :)

        <p><i>(Please don't use hard reload this page)</i></p>
      </div>
    );
  }
}

export default Dashboard;
