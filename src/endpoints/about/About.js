import React, { Component } from 'react';
import './index.scss';

class About extends Component {
 
  render() {
    return (
      <div className="About">
        <h1>Frontend developer</h1>
          <h3>Overview:</h3>

          <p>A hardworking, stress-resistant and resourceful frontend and backend developer with extensive knowledge and experience in developing web applications and console applications using PHP, JS and SQL.
            Has experience in developing applications for corporate, training, service and information purposes.</p>
            
          <h3>Skills Summary:</h3>
          
          <code>
              Programming Languages: PHP, JavaScript, SQL
              Architecture style/Technologies: REST, SOAP
              Virtualization tools: Docker, Vagrant
              Application Servers and Middleware: NGINX, Apache Web Server
              Integrated Development Environment: PHPStorm, Atom, Sublime
              Frameworks, libraries: Symfony, FOSRestBundle, JmsSerializer, Doctrine, Composer, Twig, Angular, Vue, Vuex, Gulp, Webpack, JQuery, Lodash, Bootstrap, D3 
              Platforms / OS: Mac OS, Linux (Ubuntu), Windows
              Relational Database Management Systems: MySQL, PostgreSQL
              Development Methodologies: Agile, Scrum
              Source Control Systems: GIT, Subversion
          </code>
          
          <h3>Education:</h3>
          
          <p>High Education – Belarusian State University of Informatics and Radioelectronics.</p>
      
          <h4>Contacts:</h4>
          <ul>
            <li>Telegramm: <a href="https://t.me/uvolos">@uvolos</a></li>
            <li>Phone: <span className="text-italic">+375(33)345-78-48</span></li>
          </ul>

      </div>
    );
  }
}

export default About;
