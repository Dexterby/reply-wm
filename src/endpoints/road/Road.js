import React, { Component } from 'react';
import RoadsApi from 'api/services/RoadsApi';
import './index.scss';

const roadsApi = new RoadsApi();

class Road extends Component {
  state = {
    data: [],
    filtered: [],
    query: ''
  }

  componentDidMount() {
    this.getRoads();
  }

  search = (event) => {

    this.setState({query: event.target.value});

    let q = event.target.value ? event.target.value.toUpperCase() : null;
    if (!q) {
      return false;
    }

    let resultFilter = this.state.data.filter(item => {
      return (item.name && item.name.toUpperCase().indexOf(q) !== -1) ||
        (item.displayName && item.displayName.toUpperCase().indexOf(q) !== -1) ||
        (item.statusSeverity && item.statusSeverity.toUpperCase().indexOf(q) !== -1) ||
        (item.bounds && item.bounds.toUpperCase().indexOf(q) !== -1) ||
        (item.url && item.url.toUpperCase().indexOf(q) !== -1);
    });

    this.setState({filtered: resultFilter});
  }

  getRoads() {
    return roadsApi.getRoads()
      .then(response => this.setState({data: response, filtered: response}));
  }

  render() {
    return (
      <div className="Road">
        <div className="search-input input-group mb-3">
          <input type="text" className="form-control" 
            onChange={this.search}
            placeholder="Enter the search text" aria-describedby="basic-addon2" />
          <div className="input-group-append">
            <span className="input-group-text" id="basic-addon2">Search</span>
          </div>
        </div>
        <div className="road-table table-striped">
          { this.state.filtered && !!this.state.filtered.length &&
            <table className="table">
              <thead> 
                <tr>
                  <th>id</th>
                  <th>name</th>
                  <th>status severity</th>
                  <th>bounds</th>
                  <th>url</th>
                </tr>
              </thead>
              <tbody>
                {
                  this.state.filtered.map((item, index) => (
                    <tr key={item.id}>
                      <td>
                        {`${item.id}`}
                      </td>
                      <td>
                        {`${item.displayName}`}
                      </td>
                      <td>
                        {`${item.statusSeverity}`}
                      </td>
                      <td>
                        {`${item.bounds}`}
                      </td>
                      <td>
                        {`${item.url}`}
                      </td>
                    </tr>
                  ))
                }
              </tbody>
            </table>
          }
        </div>
        <div className="pagination-block">
          <ul className="pagination">
            <li className="page-item disabled">
              <a className="page-link">Previous</a>
            </li>
            <li className="page-item"><a className="page-link">1</a></li>
            <li className="page-item"><a className="page-link">2</a></li>
            <li className="page-item"><a className="page-link">3</a></li>
            <li className="page-item">
              <a className="page-link">Next</a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default Road;
