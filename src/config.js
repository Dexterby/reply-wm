export const appConfig = {
    API : {
        URL: 'https://api.tfl.gov.uk/',
        ROUTES: {
            JWTB: 'journey/journeyresults/westminster/to/bank',
            ROAD: 'road'
        },
    }
};