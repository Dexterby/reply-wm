import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom'

import Dashboard from './endpoints/dashboard/Dashboard';
import Road from './endpoints/road/Road';
import About from './endpoints/about/About';
import Header from 'components/header/Header';
import Footer from 'components/footer/Footer';

import './App.scss';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <div className="header-block">
          <Header/>
        </div>
        <div className="main-block">
          <Switch>
            <Route exact path='/dashboard' component={Dashboard}/>
            <Route exact path='/road' component={Road}/>
            <Route exact path='/about' component={About}/>
            <Redirect to="/dashboard" />
          </Switch>
        </div>
        <div className="footer-block">
          <Footer />
        </div>
      </div>
    );
  }
}