import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import logo from 'assets/images/logo.png';
import './index.scss';

export default class Header extends Component {
  state = {
    pages: [
      {name: 'Dashboard', url: '/dashboard'},
      {name: 'Road', url: '/road'},
      {name: 'About me', url: '/about'},
    ]
  };

  render() {
    return (
      <div className="Header">
        <div className="logo">
          <a href="https://www.reply.com"><img src={logo} className="App-logo" alt="logo" /></a>
        </div>
        <ul>
          {
            this.state.pages.map(page => (
              <li key={page.url}>
                <NavLink to={page.url}>
                  {page.name}
                </NavLink>
              </li>
            ))
          }
        </ul>
      </div>
    );
  }
}
