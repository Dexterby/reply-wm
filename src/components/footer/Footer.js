import React, { Component } from 'react';
import copyright from 'assets/images/copyright.svg';
import './index.scss';

export default class Footer extends Component {
  state = {
    links: [
      {name: 'Telegram', url: 'https://t.me/uvolos'},
      {name: 'Reply WM', url: 'https://www.reply.com/'},
      {name: 'About me', url: '/about'},
    ]
  };

  render() {
    return (
      <div className="Footer">
        <div className="info">
          Copyright <img src={copyright} alt="(c)" /> by U.Volos
        </div>
        <ul>
          {
            this.state.links.map(link => (
              <li key={link.url}>
                <a href={link.url}>
                  {link.name}
                </a>
              </li>
            ))
          }
        </ul>
      </div>
    );
  }
}
